#!/bin/bash

echo "Launching TeaLeaf2d hypre with gnu compiler"

compiler="gnu"

for i in 32 #1 2 4 8 16;
do
	sbatch --nodes $i --output ./profile/${compiler}/tl_ref_${compiler}_n_$i.txt tea_job_${compiler}.pbs
done
